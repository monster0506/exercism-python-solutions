def append(list1, list2):
    return list1 + list2


def concat(lists):
    new_list = []
    for lista in lists:
        for x in lista:
            new_list.append(x)
    return new_list


def filter(function, list):
    new_list = []
    for i in range(length(list)):
        if function(list[i]):
            new_list.append(list[i])
    return new_list


def length(list):
    result = 0
    for _ in list:
        result += 1
    return result


def map(function, list):
    new_list = []
    for i in range(length(list)):
        new_list.append(function(list[i]))
    return new_list


def foldl(function, list, initial):
    for i in range(length(list)):
        initial = function(initial, list[i])
    return initial


def foldr(function, list, initial):
    list = reverse(list)
    for i in range(length(list)):
        initial = function(initial, list[i])
    return initial


def reverse(list):
    new_list = []
    for i in range(length(list) - 1, -1, -1):
        new_list.append(list[i])
    return new_list
