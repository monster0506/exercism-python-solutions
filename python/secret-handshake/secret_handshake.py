steps = {4: "wink", 3: "double blink", 2: "close your eyes", 1: "jump"}


def commands(binary_str):
    results = []
    for i in range(len(binary_str) - 1, -1, -1):
        if binary_str[i] == "1" and i != 0:
            results.append(steps[i])
    if binary_str[0] == "1":
        results = results[::-1]
    return results
