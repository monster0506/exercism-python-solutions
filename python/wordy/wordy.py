ops = {
    "plus": "+",
    "minus": "-",
    "divided": "/",
    "multiplied": "*",
}


def answer(question: str) -> int:
    question = (
        question.replace("What is ", "").replace("?", "").replace("by ", "").split()
    )
    for i in range(len(question)):
        if question[i] in ops.keys():
            question[i] = ops[question[i]]
    question.insert(0, "(")
    question.insert(4, ")")
    if any([x.isalpha() for x in question if x not in ["What", "is"]]):
        raise ValueError("unknown operation")
    try:
        return eval(" ".join(question))
    except:
        raise ValueError("syntax error")
