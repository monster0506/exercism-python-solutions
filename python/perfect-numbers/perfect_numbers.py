def classify(number):
    """A perfect number equals the sum of its positive divisors.

    :param number: int a positive integer
    :return: str the classification of the input integer
    """
    if number < 1:
        raise ValueError("Classification is only possible for positive integers.")

    factors = [1]
    for i in range(2, number):
        if number / i == number // i:
            factors.append(i)

    count = sum(factors)
    if len(factors) == 1 or count < number:
        return "deficient"
    if count > number:
        return "abundant"
    return "perfect"
