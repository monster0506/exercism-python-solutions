def find_anagrams(word: str, candidates: list):
    word = word.upper()
    results = []
    for result in candidates:
        if sorted(word) == sorted(result.upper()) and word != result.upper():
            results.append(result)
    return results
