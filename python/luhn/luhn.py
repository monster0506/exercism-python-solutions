class Luhn:
    def __init__(self, card_num: str):
        self.number = card_num.replace(" ", "")

    def valid(self):
        if not self.number.isnumeric() or len(self.number) < 2:
            return False
        new = ""
        for i in range(len(self.number) - 2, -1, -1):
            if i % 2 == len(self.number) % 2:
                val = int(self.number[i]) * 2
                if val > 9:
                    val -= 9
                new += str(val)
            else:
                new += self.number[i]
        return sum([int(x) for x in new[::-1] + self.number[-1]]) % 10 == 0
