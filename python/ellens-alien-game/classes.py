"""Solution to Ellen's Alien Game exercise."""


class Alien:
    """Create an Alien object with location x_coordinate and y_coordinate.

    Attributes
    ----------
    (class)total_aliens_created: int
    x_coordinate: int - Position on the x-axis.
    y_coordinate: int - Position on the y-axis.
    health: int - Number of health points.

    Methods
    -------
    hit(): Decrement Alien health by one point.
    is_alive(): Return a boolean for if Alien is alive (if health is > 0).
    teleport(new_x_coordinate, new_y_coordinate): Move Alien object to new coordinates.
    collision_detection(other): Implementation TBD.
    """

    total_aliens_created = 0

    def __init__(self, x, y) -> None:
        self.x_coordinate = x
        self.y_coordinate = y
        self.health = 3
        Alien.total_aliens_created += 1

    def is_alive(self) -> bool:
        return self.health > 0

    def teleport(self, x, y) -> None:
        self.x_coordinate = x
        self.y_coordinate = y

    def hit(self) -> None:
        self.health -= 1

    def collision_detection(self, other):
        if (
            other.x_coordinate == self.x_coordinate
            and other.y_coordinate == self.y_coordinate
        ):
            self.hit()
            other.hit()


# TODO:  create the new_aliens_collection() function below to call your Alien class with a list of coordinates.
def new_aliens_collection(coords):
    aliens = []
    for coord in coords:
        aliens.append(Alien(*coord))
    return aliens
