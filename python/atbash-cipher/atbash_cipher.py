from textwrap import wrap

alphabet = "abcdefghijklmnopqrstuvwxyz"
encoded = alphabet[::-1]


def encode(plain_text):
    plain_text = "".join(x.lower() for x in plain_text.upper() if x.isalnum())
    result = "".join([transpose(i, encoded, alphabet) for i in plain_text])
    return " ".join(wrap(result, 5))


def decode(ciphered_text: str):
    ciphered_text = "".join(x.lower() for x in ciphered_text.upper() if x.isalnum())

    result = [transpose(i, alphabet, encoded) for i in ciphered_text]
    return "".join(result)


def transpose(letter, alphabet_1, alphabet_2):
    if letter not in alphabet_1:
        return letter

    return alphabet_1[alphabet_2.index(letter)]
