def primes(limit):
    candidates = {x: True for x in range(2, limit + 1)}
    for i in candidates:
        if candidates[i]:
            for j in range(i * i, limit + 1, i):
                candidates[j] = False
    return [x for x in candidates if candidates[x]]
