def score(x, y):
    location = x**2 + y**2
    if location > 10**2:
        return 0
    if location > 5**2:
        return 1
    if location > 1**2:
        return 5
    return 10
