def factors(value):
    result = []
    factors = 2
    while value > 1:
        if value % factors == 0:
            value /= factors
            result.append(factors)
        else:
            factors += 1
    return result
