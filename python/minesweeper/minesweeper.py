from typing import List


def annotate(minefield: List[List[str]]):
    if minefield == []:
        return []
    else:
        rows_len = len(minefield)
        cols_len = len(minefield[0])
        for col in minefield:
            if len(col) != cols_len:
                raise ValueError("The board is invalid with current input.")
        for i in range(rows_len):
            temp = ""
            for j in range(cols_len):
                if minefield[i][j].isspace():
                    temp += helper(minefield, i, j)
                elif minefield[i][j] == "*":
                    temp += "*"
                else:
                    raise ValueError("The board is invalid with current input.")
            minefield[i] = temp
        return minefield


def helper(minefield: List[List[str]], i, j):
    directions = [[-1, -1], [1, 1], [0, 1], [1, -1], [0, -1], [-1, 0], [1, 0], [-1, 1]]
    count = 0
    for direction in directions:
        if (
            0 <= i + direction[0] <= len(minefield) - 1
            and 0 <= j + direction[1] <= len(minefield[0]) - 1
        ) and (minefield[i + direction[0]][j + direction[1]] == "*"):
            count += 1

    return " " if count == 0 else str(count)
