def square(number):
    if 1 > number or number > 64:
        raise ValueError("square must be between 1 and 64")
    return pow(2, number - 1)


def total():
    count = 0
    for i in range(1, 65):
        count += square(i)
    return count
