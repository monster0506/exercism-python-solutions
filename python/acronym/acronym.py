import string


def abbreviate(words: str):

    words = words.replace("-", " ").strip()
    words = "".join([ch for ch in words if ch not in set(string.punctuation)]).split()

    acroynm = ""
    for word in words:
        acroynm += word[0].upper()
    return acroynm
