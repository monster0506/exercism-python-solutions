import re


def is_valid(isbn: str):
    isbn = isbn.replace("-", "")
    if len(isbn) != 10:
        return False
    pattern = re.compile("[0-9]-?[0-9]{3}-?[0-9]{5}-?[0-9,X]")
    match = pattern.match(isbn)
    if not match:
        return False
    isbn = list(match.group(0))
    if isbn[-1] == "X":
        isbn[-1] = "10"
    count = 0
    for i in range(0, 10):
        count += (i + 1) * int(isbn[i])
    if count % 11 == 0:
        return True
    return False
