def sum_of_multiples(limit, multiples):
    new_set = set()
    for k in multiples:
        for i in range(1, limit):
            multiplied = k * i
            if multiplied >= limit:
                break
            new_set.add(multiplied)

    return sum(new_set)
