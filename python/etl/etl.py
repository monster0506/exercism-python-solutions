def transform(legacy_data):
    result = {i.lower(): x for x in legacy_data for i in legacy_data[x]}
    return result
