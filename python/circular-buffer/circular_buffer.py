class BufferFullException(BufferError):
    """Exception raised when CircularBuffer is full.

    message: explanation of the error.

    """

    def __init__(self, message):
        self.message = message


class BufferEmptyException(BufferError):
    """Exception raised when CircularBuffer is empty.

    message: explanation of the error.

    """

    def __init__(self, message):
        self.message = message


class CircularBuffer:
    def __init__(self, capacity):
        self.capacity = capacity
        self.items = []

    def read(self):
        if len(self.items) == 0:
            raise BufferEmptyException("Circular buffer is empty")
        return self.items.pop(0)

    def write(self, data):
        if len(self.items) >= self.capacity:
            raise BufferFullException("Circular buffer is full")
        self.items.append(data)

    def overwrite(self, data):
        if len(self.items) < self.capacity:
            self.write(data)
        else:
            self.read()
            self.write(data)

    def clear(self):
        self.items = []
