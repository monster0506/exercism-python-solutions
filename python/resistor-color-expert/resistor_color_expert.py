source = {
    "black": [0, "ohms"],
    "brown": [1, "ohms", "±1%"],
    "red": [2, "kiloohms", "±2%"],
    "orange": [3, "kiloohms"],
    "yellow": [4, "kiloohms"],
    "green": [5, "megaohms", "±0.5%"],
    "blue": [6, "megaohms", "±0.25%"],
    "violet": [7, "megaohms", "±0.1%"],
    "grey": [8, "gigaohms", "±0.05%"],
    "white": [9, "gigaohms"],
    "gold": [10, None, "±5%"],
    "silver": [11, None, "±10%"],
}


def resistor_label(colors):
    if len(colors) == 1:
        return f"{source[colors[0]][0]} {source[colors[0]][1]}"
    return f"{label(colors[:-1], len(colors)-2)} {source[colors[-1]][2]}"


def label(colors, size_initial):
    initial = ""
    for i in range(size_initial):
        initial += str(source[colors[i]][0])
    initial = str(int(str(initial)))
    multiplier = source[colors[-1]][0]
    zeros = "0" * (multiplier)
    ending = "ohms"
    number = int(initial + zeros)
    if len(str(number)) > 9:
        ending = "gigaohms"
        number /= 100000000
    elif len(str(number)) > 6:
        ending = "megaohms"
        number /= 1000000
    elif len(str(number)) > 3:
        ending = "kiloohms"
        number /= 1000
    if ".0" in str(number)[1:]:
        number = int(number)
    return f"{number} {ending}"
