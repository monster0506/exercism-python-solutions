import concurrent.futures
from collections import defaultdict


def merge_dictionaries(dicts):
    combined = defaultdict(int)
    for d in dicts:
        for key, value in d.items():
            combined[key] += value
    return dict(combined)


def thread_function(stuff: str):
    results = {}
    stuff = "".join([ch.lower() for ch in stuff if ch.isalpha()])
    for letter in stuff:
        if letter in results:
            results[letter] += 1
        else:
            results[letter] = 1
    return results


def frequency(text):
    if len(text) == 0:
        return {}
    results = {}
    with concurrent.futures.ThreadPoolExecutor(max_workers=len(text)) as executor:
        futures = [executor.submit(thread_function, value) for value in text]

    results = [future.result() for future in concurrent.futures.as_completed(futures)]
    return merge_dictionaries(results)
