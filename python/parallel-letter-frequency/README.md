# Instructions

Count the frequency of letters in texts using parallel computation.

The function should return a dictionary in the format {\<letter>: \<count>}

Case does not matter, i.e., `frequency(['Mm']) == frequency(['mm'])`

Non-alphabetic characters and whitespace should be ignored.


# Example
```python
>>> frequency(["abc", "baccd"])

{"a": 2, "b": 2, "c": 3, "d": 4}
```

Python is single-threaded by nature, but is capable of multiple threading using the [`threading`](https://docs.python.org/3/library/threading.html) or [`concurrent.futures`](https://docs.python.org/3/library/concurrent.futures.html) modules.

## Concurency vs. Parallelism

Here's a quick definition for each that illustrates the diferences between the two:

    Concurrency is when two or more tasks can start, run and complete in overlapping time periods, being executed by the same processing unit.
    Parallelism is when two or more tasks can start and run at the same time, being executed independently of eachother by separate processing units.

For the sake of completeness, here's a definition for synchronous execution:

    Synchronous execution is when a task has to wait for another running task to complete, before it can run.

## Example
Here is a simple demonstration of `threading` (taken from [here](https://www.geeksforgeeks.org/multithreading-python-set-1/)).

```python
import threading 
def print_cube(num):
    print("Cube: {}" .format(num * num * num))
 
 
def print_square(num):
    print("Square: {}" .format(num * num))
 
 
if __name__ =="__main__":
    t1 = threading.Thread(target=print_square, args=(10,))
    t2 = threading.Thread(target=print_cube, args=(10,))
 
    t1.start()
    t2.start()
 
    t1.join()
    t2.join()
 
    print("Done!")

```



