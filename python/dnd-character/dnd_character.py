from random import randint


class Character:
    def __init__(self):
        self.strength = self.ability()
        self.dexterity = self.ability()
        self.constitution = self.ability()
        self.intelligence = self.ability()
        self.wisdom = self.ability()
        self.charisma = self.ability()
        self.hitpoints = 10 + modifier(self.constitution)

    @classmethod
    def ability(self):
        values = [
            randint(1, 6),
            randint(1, 6),
            randint(1, 6),
            randint(1, 6),
        ]

        return sum(values) - min(values)


def modifier(num):
    return (num - 10) // 2
