def roman(number):
    numbers = {
        1: "I",
        4: "IV",
        5: "V",
        9: "IX",
        10: "X",
        40: "XL",
        50: "L",
        90: "XC",
        100: "C",
        400: "CD",
        500: "D",
        900: "CM",
        1000: "M",
    }

    i = len(numbers) - 1
    result = ""
    while number:
        current = list(numbers.keys())[i]
        div = number // current
        number = number % current
        while div:
            result += numbers[current]
            div -= 1
        i -= 1
    return result
