consonants = "bcdfghjklmnpqrstvwxz"


def translate(text: str):
    result = []
    for word in text.split():
        i = 0
        while word[i] in consonants or (word[i] == "y" and i == 0):
            if word[i : i + 2] in ["xr", "yt"]:
                break
            if word[i : i + 2] == "qu":
                i += 1
            i += 1
        c_part = word[:i]
        rest = word[i:]
        if len(c_part) == 0:
            result.append(f"{rest}ay")
        else:
            result.append(f"{rest}{c_part}ay")
    return " ".join(result)
