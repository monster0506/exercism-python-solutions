def equilateral(sides):
    if not is_triangle(sides):
        return False
    return sides[0] == sides[1] == sides[2]


def isosceles(sides):
    if not is_triangle(sides):
        return False
    if equilateral(sides):
        return True
    return sides[0] == sides[1] or sides[1] == sides[2] or sides[0] == sides[2]


def scalene(sides):
    if not is_triangle(sides):
        return False
    return not isosceles(sides)


def is_triangle(sides):
    if 0 in sides:
        return False
    side1 = sides[0] + sides[1] >= sides[2]
    side2 = sides[1] + sides[2] >= sides[0]
    side3 = sides[0] + sides[2] >= sides[1]
    return side1 and side2 and side3
