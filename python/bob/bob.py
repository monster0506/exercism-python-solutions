def response(hey_bob: str):
    return_values = [
        "Sure.",
        "Whoa, chill out!",
        "Calm down, I know what I'm doing!",
        "Fine. Be that way!",
        "Whatever.",
    ]
    hey_bob = hey_bob.strip()
    is_shout = hey_bob.isupper()
    is_whitespace = hey_bob == ""
    if is_whitespace:
        return return_values[3]
    is_question = hey_bob[-1] == "?"
    if is_question and is_shout:
        return return_values[2]
    if is_question:
        return return_values[0]
    if is_shout:
        return return_values[1]
    return return_values[4]
