source = {
    "black": [0, "ohms"],
    "brown": [1, "ohms"],
    "red": [2, "kiloohms"],
    "orange": [3, "kiloohms"],
    "yellow": [4, "kiloohms"],
    "green": [5, "megaohms"],
    "blue": [6, "megaohms"],
    "violet": [7, "megaohms"],
    "grey": [8, "gigaohms"],
    "white": [9, "gigaohms"],
}


def label(colors):
    initial = str(int(str(source[colors[0]][0]) + str(source[colors[1]][0])))
    multiplier = source[colors[2]][0]
    ending = source[colors[2]][1]
    zeros = "0" * (multiplier % 3)
    if len(initial + zeros) > 3:
        zeros = ""
        initial = initial[0]
    return f"{initial+zeros} {ending}"