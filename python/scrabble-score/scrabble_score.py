def score(word):

    if len(word) == 0:
        return 0
    count = 0
    for letter in word.upper():
        for key, value in letters.items():
            if letter in value:
                count += key
    return count


letters = {
    1: ["A", "E", "I", "O", "U", "L", "N", "R", "S", "T"],
    2: ["D", "G"],
    3: ["B", "C", "M", "P"],
    4: ["F", "H", "V", "W", "Y"],
    5: ["K"],
    8: ["J", "X"],
    10: ["Q", "Z"],
}
