class Allergies:

    def __init__(self, score):
        self.score = score

    def allergic_to(self, item):
        return bool(self.score & allergies[item])

    @property
    def lst(self):
        results = [x for x in allergies if self.allergic_to(x)]
        return results


allergies = {
    "eggs": 1,
    "peanuts": 2,
    "shellfish": 4,
    "strawberries": 8,
    "tomatoes": 16,
    "chocolate": 32,
    "pollen": 64,
    "cats": 128,
}
