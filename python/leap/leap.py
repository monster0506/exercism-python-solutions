def leap_year(year):
    divide_by_4 = year % 4 == 0
    divide_by_100 = year % 100 == 0
    divide_by_400 = year % 400 == 0
    
    return divide_by_4 and (divide_by_100 == divide_by_400)
