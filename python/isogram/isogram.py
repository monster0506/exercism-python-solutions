def is_isogram(string: str):
    string = "".join([x.upper() for x in string if x.isalpha()])
    return sorted(list(string)) == sorted(list(set(string)))
