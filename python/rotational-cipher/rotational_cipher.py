def rotate(text: str, key):
    letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    rotated = letters[key:] + letters[:key]
    rotated_lower = letters[key:] + letters[:key]
    rotated_lower = [letter.lower() for letter in letters[key:]] + [
        letter.lower() for letter in letters[:key]
    ]
    result = ""
    for letter in text:
        if letter.islower():
            result += rotated_lower[letters.index(letter.upper())]
        elif letter.isupper():
            result += rotated[letters.index(letter)]
        else:
            result += letter
    return result
