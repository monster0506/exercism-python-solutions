def is_pangram(sentence: str):
    letters = set("ABCDEFGHIJKLMNOPQRSTUVWXYZ")
    sentence = set([x.upper() for x in sentence if x.isalpha()])
    return sentence == letters
