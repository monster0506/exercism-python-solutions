def is_paired(input_string):
    if len(input_string) == 0:
        return True
    input_string = "".join([x for x in input_string if x in "(){}[]"])
    if (
        len(input_string) % 2 == 1
        or input_string[0] in ")}]"
        or input_string[-1] in "[{("
    ):
        return False
    stack = []
    dic = {")": "(", "}": "{", "]": "["}
    for c in input_string:
        if c in dic.values():
            stack.append(c)
        else:
            if not stack or stack[-1] != dic[c]:
                return False
            stack.pop()
    return not stack
