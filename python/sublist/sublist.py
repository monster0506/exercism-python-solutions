"""
This exercise stub and the test suite contain several enumerated constants.

Enumerated constants can be done with a NAME assigned to an arbitrary,
but unique value. An integer is traditionally used because it’s memory
efficient.
It is a common practice to export both constants and functions that work with
those constants (ex. the constants in the os, subprocess and re modules).

You can learn more here: https://en.wikipedia.org/wiki/Enumerated_type
"""

# Possible sublist categories.
# Change the values as you see fit.
SUBLIST = 1
SUPERLIST = 2
EQUAL = 3
UNEQUAL = 4


def sublist(list_one, list_two):
    if list_one == list_two:
        return EQUAL
    if helper(list_one, list_two, abs(len(list_one) - len(list_two)) + 1):
        return SUPERLIST
    if helper(list_two, list_one, abs(len(list_one) - len(list_two)) + 1):
        return SUBLIST
    return UNEQUAL


def helper(list_1, list_2, size):
    for i in range(size):
        if not list_2 or list_2 == list_1[i : i + len(list_2)]:
            return True
    return False
