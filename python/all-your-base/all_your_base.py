def rebase(input_base, digits, output_base):
    if input_base < 2:
        raise ValueError("input base must be >= 2")
    if output_base < 2:
        raise ValueError("output base must be >= 2")
    result = 0
    digits = digits[::-1]
    for i in range(len(digits)):
        value = int(digits[i])
        if not (0 <= value and value < input_base):
            raise ValueError("all digits must satisfy 0 <= d < input base")
        result += value * (input_base**i)
    if result == 0:
        return [0]
    new = []

    value = result
    while value:
        a = value % output_base
        value = value // output_base
        new.append(a)

    return new[::-1]
