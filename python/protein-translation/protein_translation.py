def proteins(strand):
    if len(strand) % 3 != 0:
        return "AUGAU"
    result = []
    for i in range(0, len(strand), 3):
        codon = strand[i : i + 3]
        if codon in codons["STOP"]:
            return result
        for key, value in codons.items():
            if codon in value:
                result.append(key)
    return result


codons = {
    "Methionine": ["AUG"],
    "Phenylalanine": ["UUU", "UUC"],
    "Leucine": ["UUA", "UUG"],
    "Serine": ["UCU", "UCC", "UCA", "UCG"],
    "Tyrosine": ["UAU", "UAC"],
    "Cysteine": ["UGU", "UGC"],
    "Tryptophan": ["UGG"],
    "STOP": ["UAA", "UAG", "UGA"],
}
