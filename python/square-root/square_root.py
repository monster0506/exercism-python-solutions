def square_root(number):
    if number in [0, 1]:
        return number
    a = number + 1
    b = number
    while a > b:
        a = b
        b = (b + number / b) / 2
    return a
