#include "sieve.h"
#include <map>
// #include <iostream>

namespace sieve {
std::vector<int> primes(int num) {
  std::vector<int> result;
  std::vector<int> numbers;
  std::map<int, bool> candidates;
  for (int i = 2; i < num + 1; i++) {
    candidates[i] = true;
    numbers.push_back(i);
  }

  for (int i : numbers) {
    if (candidates[i]) {
      for (int j = i * i; j < num + 1; j += i) {
        candidates[j] = false;
      }
    }
  }

  for (int i : numbers) {
    if (candidates[i]) {
      result.push_back(i);
    }
  }

  return result;
}

} // namespace sieve
