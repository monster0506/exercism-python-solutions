#include "pangram.h"
#include <cctype>
#include <unordered_set>

namespace pangram {
std::unordered_set<char> letters = {
    'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
    'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
};
bool is_pangram(std::string str) {
  std::unordered_set<char> newSet;
  for (char c : str) {
    if (isalpha(c)) {
      newSet.insert(tolower(c));
    }
  }
  return newSet == letters;
}
} // namespace pangram
