#include <array>
#include <string>
#include <vector>

// Round down all provided student scores.
std::vector<int> round_down_scores(std::vector<double> student_scores) {
  std::vector<int> newArray{};
  for (double i : student_scores) {
    newArray.emplace_back(i);
  }
  return newArray;
}

// Count the number of failing students out of the group provided.
int count_failed_students(std::vector<int> student_scores) {
  int result{0};
  for (int i : student_scores) {
    if (i <= 40)
      ++result;
  }
  return result;
}

// Determine how many of the provided student scores were 'the best' based on
// the provided threshold.
std::vector<int> above_threshold(std::vector<int> student_scores,
                                 int threshold) {
  std::vector<int> newArray{};
  for (int i : student_scores) {
    if (i >= threshold) {
      newArray.emplace_back(i);
    }
  }
  return newArray;
}

// Create a list of grade thresholds based on the provided highest grade.
std::array<int, 4> letter_grades(int highest_score) {
  std::array<int, 4> answer{};
  int gap = (int)((highest_score - 40) / 4);
  for (int i = 0; i < answer.size(); i++) {
    answer[i] = gap * i + 41;
  }
  return answer;
}

// Organize the student's rank, name, and grade information in ascending order.
std::vector<std::string>
student_ranking(std::vector<int> student_scores,
                std::vector<std::string> student_names) {
  std::vector<std::string> answer{};
  std::string current{};
  for (int i = 0; i < student_names.size(); i++) {
    current = std::to_string(i + 1) + ". " + student_names[i] + ": " +
              std::to_string(student_scores[i]);
    answer.emplace_back(current);
  }

  return answer;
}

// Create a string that contains the name of the first student to make a perfect
// score on the exam.
std::string perfect_score(std::vector<int> student_scores,
                          std::vector<std::string> student_names) {
  std::string result{};
  for (int i{}; i < student_scores.size(); i++) {
    if (student_scores[i] == 100) {
      result = student_names[i];
      break;
    }
  }
  return result;
}
