#include "hexadecimal.h"
#include <cmath>

std::string options = "0123456789abcdef";
namespace hexadecimal {
int convert(std::string str) {
  int result{};
  int i = str.size();
  for (char c : str) {
    if (options.find(c) == std::string::npos) {
      return 0;
    }
    int value = options.find_first_of(c);
    int multiplier = pow(16, i - 1);
    result += value * multiplier;
    i--;
  }
  return result;
}

} // namespace hexadecimal
