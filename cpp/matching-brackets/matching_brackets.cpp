#include "matching_brackets.h"
#include <map>
#include <set>
#include <stack>

namespace matching_brackets {

char pop(std::stack<char> &stack) {
  char c = stack.top();
  stack.pop();
  return c;
}
std::map<char, char> brackets{
    {'}', '{'},
    {']', '['},
    {')', '('},
};
std::set<char> open{'{', '[', '('};

bool check(std::string str) {
  if (str.size() == 0)
    return true;
  std::string newString{};
  for (char c : str) {
    switch (c) {
    case '(':
    case ')':
    case '[':
    case ']':
    case '{':
    case '}':
      newString.push_back(c);
      break;
    default:
      break;
      continue;
    }
  }
  str = newString;
  if (str.size() % 2 == 1 ||
      (str.front() == ')' || str.front() == ']' || str.front() == '}') ||
      (str.back() == '(' || str.back() == '[' || str.back() == '{'))
    return false;
  std::stack<char> stack;
  for (char c : str) {
    if (open.find(c) != open.end()) {
      stack.push(c);
    } else if (brackets.find(c) != brackets.end()) {
      if (stack.size() == 0 || brackets.at(c) != pop(stack)) {
        return false;
      }
    }
  }
  return stack.size() == 0;
}

} // namespace matching_brackets
