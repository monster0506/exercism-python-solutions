#include "leap.h"

namespace leap {
bool is_leap_year(int year) {
  bool divide_by_4 = year % 4 == 0;
  bool divide_by_100 = year % 100 == 0;
  bool divide_by_400 = year % 400 == 0;

  return divide_by_4 && (divide_by_100 == divide_by_400);
}
} // namespace leap
