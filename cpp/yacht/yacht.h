#pragma once

#include <string>
#include <vector>
namespace yacht {
int score(std::vector<int> game, std::string str);

} // namespace yacht
