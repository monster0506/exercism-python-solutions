#include "yacht.h"
#include <algorithm>
#include <numeric>

namespace yacht {
int helper(std::vector<int> game, int number) {
  int count{};
  for (int k : game) {
    if (k == number) {
      count++;
    }
  }
  return count * number;
}

int sum(std::vector<int> game) {
  int result{};
  for (int k : game) {
    result += k;
  }
  return result;
}
int score(std::vector<int> game, std::string str) {
  if (str == "yacht") {
    int j = game.at(0);
    for (int k : game)
      if (k != j)
        return 0;
    return 50;
  }
  if (str == "ones")
    return helper(game, 1);

  if (str == "twos")
    return helper(game, 2);

  if (str == "threes")
    return helper(game, 3);

  if (str == "fours")
    return helper(game, 4);

  if (str == "fives")
    return helper(game, 5);

  if (str == "sixes")
    return helper(game, 6);

  std::sort(game.begin(), game.end());
  int all = sum(game);
  if (str == "four of a kind") {
    int front = game.front();
    int back = game.back();
    int frontCount{};
    int backCount{};
    for (int k : game) {
      if (k == front)
        frontCount++;
      if (k == back)
        backCount++;
    }
    if (frontCount >= 4)
      return front * 4;
    if (backCount >= 4)
      return back * 4;
    return 0;
  }

  if (str == "full house") {
    const int front{game.at(0)};
    const int back{game.at(4)};
    if (front == back)
      return false;
    std::vector<int> full{front, front, back, back, back};
    if (game == full)
      return all;
    full[2] = front;
    return game == full ? all : 0;
  }
  if (str == "little straight") {
    std::vector<int> test = {1, 2, 3, 4, 5};
    return test == game ? 30 : 0;
  }
  if (str == "big straight") {
    std::vector<int> test = {2, 3, 4, 5, 6};
    return test == game ? 30 : 0;
  }
  if (str == "choice") {
    int result{};
    for (int k : game)
      result += k;
    return result;
  }
  return 1;
}

} // namespace yacht
