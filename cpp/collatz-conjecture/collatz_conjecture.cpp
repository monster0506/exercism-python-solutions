#include "collatz_conjecture.h"
#include <stdexcept>

namespace collatz_conjecture {
int steps(int a) {
  if (a <= 0)
    throw std::domain_error("Error?");

  int count = 0;
  while (a != 1) {
    count += 1;
    if (a % 2 == 0) {
      a /= 2;
      continue;
    }
    a = 3 * a + 1;
  }
  return count;
}

} // namespace collatz_conjecture
