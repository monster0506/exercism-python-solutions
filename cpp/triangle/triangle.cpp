#include "triangle.h"
#include <stdexcept>

namespace triangle {
bool is_triangle(double a, double b, double c) {
  if (a <= 0 || b <= 0 || c <= 0)
    return false;
  if (a + b >= c && b + c >= a && a + c >= b)
    return true;
  return false;
}
flavor kind(double a, double b, double c) {
  if (!is_triangle(a, b, c))
    throw std::domain_error("Failure");
  if (a == b && a == c)
    return flavor::equilateral;
  if (a == b || b == c || a == c)
    return flavor::isosceles;
  return flavor::scalene;
}

} // namespace triangle
