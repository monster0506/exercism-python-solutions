#include "bob.h"
#include <algorithm>
#include <cctype>
#include <string>
#include <vector>

namespace bob {
std::vector<std::string> responses = {
    "Sure.",
    "Whoa, chill out!",
    "Calm down, I know what I'm doing!",
    "Fine. Be that way!",
    "Whatever.",
};

std::string hey(std::string str) {
  bool lower = false;
  bool upper = false;
  for (char c : str) {
    if (islower(c))
      lower = true;
    if (isupper(c))
      upper = true;
  }
  str.erase(remove_if(str.begin(), str.end(), isspace), str.end());
  bool is_shout = upper && !lower;
  bool is_whitespace = str == "";
  if (is_whitespace) {
    return responses[3];
  }
  if (str.back() == '?' && is_shout) {
    return responses[2];
  }
  if (str.back() == '?') {
    return responses[0];
  }
  if (is_shout) {
    return responses[1];
  }
  return responses[4];

  return str + "     " + std::to_string(is_shout);
}

} // namespace bob
