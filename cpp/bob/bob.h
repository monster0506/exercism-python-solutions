#if !defined(BOB_H)
#define BOB_H
#include <string>

namespace bob {
std::string hey(std::string str);

}; // namespace bob

#endif // BOB_H
