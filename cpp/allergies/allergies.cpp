#include "allergies.h"
#include <map>
#include <vector>

namespace allergies {
std::vector<std::string>
extract_keys(std::map<std::string, int> const &input_map) {
  std::vector<std::string> retval;
  for (auto const &element : input_map) {
    retval.push_back(element.first);
  }
  return retval;
}

std::map<std::string, int> allergies = {

    {"eggs", 1},      {"peanuts", 2},    {"shellfish", 4}, {"strawberries", 8},
    {"tomatoes", 16}, {"chocolate", 32}, {"pollen", 64},   {"cats", 128},

};
allergy_test::allergy_test(int i) { score = i; }
bool allergy_test::is_allergic_to(std::string allergy) {
  return (score & allergies[allergy]);
}
std::unordered_set<std::string> allergy_test::get_allergies() {
  std::unordered_set<std::string> result;
  for (std::string x : extract_keys(allergies))
    if (this->is_allergic_to(x))
      result.insert(x);
  return result;
}

} // namespace allergies
