#include <unordered_set>
#if !defined(ALLERGIES_H)
#define ALLERGIES_H

#include <string>
namespace allergies {
class allergy_test {
public:
  allergy_test(int i);
  bool is_allergic_to(std::string allergy);
  std::unordered_set<std::string> get_allergies();
  int score;
};

} // namespace allergies

#endif // ALLERGIES_H
