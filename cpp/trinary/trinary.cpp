#include "trinary.h"

namespace trinary {
int to_decimal(std::string trinary)

{
  int result{0};
  for (char c : trinary) {
    if (c >= '0' && c <= '2') {
      result = result * 3 + c - '0';
      continue;
    }
    return 0;
  }

  return result;
}
} // namespace trinary
