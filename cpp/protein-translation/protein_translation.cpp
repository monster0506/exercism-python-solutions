#include "protein_translation.h"

namespace protein_translation {
std::map<std::string, std::string> codons{
    {"AUG", "Methionine"}, {"UUU", "Phenylalanine"}, {"UUC", "Phenylalanine"},
    {"UUA", "Leucine"},    {"UUG", "Leucine"},       {"UCU", "Serine"},
    {"UCC", "Serine"},     {"UCA", "Serine"},        {"UCG", "Serine"},
    {"UAU", "Tyrosine"},   {"UAC", "Tyrosine"},      {"UGU", "Cysteine"},
    {"UGC", "Cysteine"},   {"UGG", "Tryptophan"},    {"UAA", "STOP"},
    {"UAG", "STOP"},       {"UGA", "STOP"},
};
std::vector<std::string> proteins(std::string input) {
  std::vector<std::string> answer{};
  for (unsigned i{0}; i < input.length(); i += 3) {
    std::string found = codons.at(input.substr(i, 3));
    if (found == "STOP") {
      break;
    }
    answer.emplace_back(found);
  }
  return answer;
}
} // namespace protein_translation
