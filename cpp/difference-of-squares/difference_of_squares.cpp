#include "difference_of_squares.h"
#include <cmath>
namespace difference_of_squares {
int square_of_sum(int number) { return pow((number * (number + 1) / 2), 2); }

int sum_of_squares(int number) {
  return (number * (number + 1) * (2 * number + 1)) / 6;
}

int difference(int number) {
  return abs(sum_of_squares(number) - square_of_sum(number));
}

} // namespace difference_of_squares
