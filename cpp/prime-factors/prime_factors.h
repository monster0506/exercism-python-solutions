#include <vector>
#if !defined(PRIME_FACTORS_H)
#define PRIME_FACTORS_H

namespace prime_factors {
std::vector<int> of(int num);

} // namespace prime_factors

#endif // PRIME_FACTORS_H
