#include "prime_factors.h"

namespace prime_factors {
std::vector<int> of(int num) {
  std::vector<int> result;
  int factors = 2;
  while (num > 1) {
    if (num % factors == 0) {
      num /= factors;
      result.push_back(factors);
    } else {
      factors++;
    }
  }

  return result;
}

} // namespace prime_factors
