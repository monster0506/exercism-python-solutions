#include "high_scores.h"

#include <algorithm>

namespace arcade {

std::vector<int> HighScores::list_scores() { return this->scores; }

int HighScores::latest_score() { return this->latest; }

int HighScores::personal_best() {
  return *std::max_element(scores.begin(), scores.end());
}

std::vector<int> HighScores::top_three() {
  std::vector<int> copy = this->scores;
  std::sort(copy.begin(), copy.end());
  std::vector<int> result;
  int max = 3 <= copy.size() ? 3 : copy.size();
  for (int i = 0; i < max; i++) {
    result.push_back(copy.at(copy.size() - i - 1));
  }
  return result;
}

void HighScores::add_score(std::vector<int> newScores) {
  this->scores.insert(scores.end(), newScores.begin(), newScores.end());
  this->latest = newScores.back();
}
void HighScores::add_score(int newScores) {
  this->scores.push_back(newScores);
  this->latest = newScores;
}

} // namespace arcade
