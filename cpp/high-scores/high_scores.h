#pragma once

#include <vector>

namespace arcade {

class HighScores {
private:
  std::vector<int> scores;
  int latest;

public:
  HighScores(std::vector<int> scores) : scores(scores), latest(scores.back()){};

  std::vector<int> list_scores();
  void add_score(std::vector<int> newScores);
  void add_score(int newScores);

  int latest_score();

  int personal_best();

  std::vector<int> top_three();
};

} // namespace arcade
