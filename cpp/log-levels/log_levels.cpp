#include <string>
namespace log_line {
std::string message(std::string line) {
  int position = line.find(":");
  std::string message = line.substr(position + 2);
  return message;
}

std::string log_level(std::string line) {
  int position1 = line.find("[");
  int position2 = line.find("]");
  std::string level = line.substr(position1 + 1, position2 - position1 - 1);
  return level;
}

std::string reformat(std::string line) {
  std::string mess = message(line);
  std::string level = log_level(line);
  std::string reformatted = mess + " (" + level + ")";
  return reformatted;
}
} // namespace log_line
