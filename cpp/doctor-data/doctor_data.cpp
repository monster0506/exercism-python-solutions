#include "doctor_data.h"
namespace heaven {
Vessel::Vessel(std::string name, int generation,
               star_map::System current_system)
    : name{std::move(name)}, generation{generation},
      current_system{current_system}, busters{0} {}
Vessel Vessel::replicate(std::string name) {
  return Vessel(name, this->generation + 1, current_system);
}
void Vessel::make_buster() { this->busters++; }
bool Vessel::shoot_buster() {
  if (this->busters <= 0)
    return false;
  this->busters--;
  return true;
}
std::string get_older_bob(Vessel b1, Vessel b2) {
  if (b1.generation > b2.generation)
    return b2.name;
  return b1.name;
}
bool in_the_same_system(Vessel b1, Vessel b2) {
  return b1.current_system == b2.current_system;
}

} // namespace heaven
