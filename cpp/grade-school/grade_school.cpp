#include "grade_school.h"
#include <algorithm>

namespace grade_school {
const std::map<int, std::vector<std::string>> &school::roster() const {
  return roster_;
}
void school::add(std::string name, int grade) {
  roster_[grade].push_back(name);
  std::sort(roster_[grade].begin(), roster_[grade].end());
}
const std::vector<std::string> school::grade(int grade) const {
  if (roster_.find(grade) != roster_.end())
    return roster_.find(grade)->second;
  return std::vector<std::string>();
}

} // namespace grade_school
