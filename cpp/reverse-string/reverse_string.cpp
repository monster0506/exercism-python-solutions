#include "reverse_string.h"

namespace reverse_string {

std::string reverse_string(std::string str) {
  std::string result{};
  for (char c : str) {
    result.insert(0, 1, c);
  }
  return result;
}
} // namespace reverse_string
