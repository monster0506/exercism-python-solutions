#include "armstrong_numbers.h"
#include <cmath>
#include <string>

namespace armstrong_numbers {
bool is_armstrong_number(int num) {

  std::string number = std::to_string(num);
  int length = number.size();
  int sum{};
  for (char c : number) {
    sum += pow(c - '0', length);
  }
  return sum == num;
}

} // namespace armstrong_numbers
