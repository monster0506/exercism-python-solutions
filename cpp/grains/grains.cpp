#include "grains.h"
#include <cmath>

namespace grains {
std::uint64_t square(int num) { return ((std::uint64_t)1 << (num - 1)); }
std::uint64_t total() {
  int count = 0;
  for (int i = 1; i < 65; i++) {
    count += square(i);
  }
  return count;
}

} // namespace grains
