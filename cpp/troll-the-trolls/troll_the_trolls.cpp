#include <iostream>
#include <string>
namespace hellmath {

// account types: `troll`, `guest`, `user`, and `mod`.
enum class AccountStatus {
  troll,
  guest,
  user,
  mod,
};

// permission types: `read`, `write`, and `remove`.
enum class Action { read, write, remove };

// of `AccountStatus` and returns a `bool`. The first argument is the status of
// the poster, the second one is the status of the viewer.
bool display_post(AccountStatus poster, AccountStatus viewer) {
  return !((viewer != AccountStatus::troll) && poster == AccountStatus::troll);
}

// `Action` as a first argument and an `AccountStatus` to check against. It
// should return a `bool`.
bool permission_check(Action action, AccountStatus account) {
  if (account == AccountStatus::mod)
    return true;
  if (account == AccountStatus::guest && action != Action::read) {
    return false;
  }
  if (action == Action::remove) {
    return false;
  }
  return true;
}

// checks if two players can join the same game. The function has two parameters
// of type `AccountStatus` and returns a `bool`.
bool valid_player_combination(AccountStatus p1, AccountStatus p2) {
  if (p1 == AccountStatus::guest || p2 == AccountStatus::guest)
    return false;
  if (p1 == p2 && p2 == AccountStatus::troll)
    return true;
  if (p1 == AccountStatus::troll || p2 == AccountStatus::troll)
    return false;
  return true;
}

// `AccountStatus` arguments and returns `true`, if and only if the first
// account has a strictly higher priority than the second.
bool has_priority(AccountStatus p1, AccountStatus p2) { return p1 > p2; }

} // namespace hellmath
