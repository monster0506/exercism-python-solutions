#include "kindergarten_garden.h"
#include <algorithm>
#include <vector>

namespace kindergarten_garden {

Plants helper(char c) {
  switch (c) {
  case 'G':
    return Plants::grass;
  case 'C':
    return Plants::clover;
  case 'R':
    return Plants::radishes;
  case 'V':
  default:
    return Plants::violets;
  }
}

std::vector<std::string> names{"Alice",  "Bob",    "Charlie", "David",
                               "Eve",    "Fred",   "Ginny",   "Harriet",
                               "Ileana", "Joseph", "Kincaid", "Larry"};
std::array<Plants, 4> plants(std::string input, std::string name) {
  std::vector<std::string>::iterator it =
      std::find(names.begin(), names.end(), name);
  int position = it - names.begin();
  int newLinePos = input.find('\n');
  std::string top = input.substr(0, newLinePos);
  std::string bottom = input.substr(newLinePos + 1, input.size() - newLinePos);
  std::string topPlants = top.substr(position * 2, 2);
  std::string bottomPlants = bottom.substr(position * 2, 2);
  std::string all = topPlants + bottomPlants;
  std::array<Plants, 4> result;

  for (int i = 0; i < 4; i++) {
    char c = all[i];
    result[i] = helper(c);
  }

  return result;
}
} // namespace kindergarten_garden
