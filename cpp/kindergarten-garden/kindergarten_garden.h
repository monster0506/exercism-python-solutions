#pragma once

#include <array>
#include <string>
#include <vector>
namespace kindergarten_garden {
enum Plants { clover, grass, violets, radishes };
std::array<Plants, 4> plants(std::string input, std::string Names);

} // namespace kindergarten_garden
