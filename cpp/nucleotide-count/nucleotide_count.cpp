#include "nucleotide_count.h"
#include <stdexcept>

namespace nucleotide_count {
const std::map<char, int> count(std::string a) {
  std::map<char, int> result{{'A', 0}, {'C', 0}, {'G', 0}, {'T', 0}};
  for (char c : a)
    if (result.find(c) != result.end())
      result[c]++;
    else
      throw std::invalid_argument("Invalid!");
  return result;
}
} // namespace nucleotide_count
