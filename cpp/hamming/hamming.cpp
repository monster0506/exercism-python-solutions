#include "hamming.h"
#include <stdexcept>

namespace hamming {
int compute(std::string a, std::string b) {
  if (a.size() != b.size())
    throw std::domain_error("Error!");
  int count{};
  for (int i{0}; i < (int)a.size(); i++)
    if (a.at(i) != b.at(i))
      count++;
  return count;
}

} // namespace hamming
