#include "rna_transcription.h"
#include <map>

namespace rna_transcription {

std::map<char, char> pairs = {
    {'G', 'C'},
    {'C', 'G'},
    {'A', 'U'},
    {'T', 'A'},

};

std::string to_rna(std::string dna) {
  for (char &c : dna) {
    c = to_rna(c);
  }
  return dna;
}
char to_rna(char dna) { return pairs.at(dna); }

} // namespace rna_transcription
