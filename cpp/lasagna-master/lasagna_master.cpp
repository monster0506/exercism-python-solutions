#include "lasagna_master.h"

namespace lasagna_master {
int preparationTime(std::vector<std::string> layers, int minutes) {
  return layers.size() * minutes;
}

struct amount quantities(std::vector<std::string> layers) {
  double sauce = 0;
  int noodles = 0;
  for (std::string layer : layers) {
    if (layer == "sauce")
      sauce += 0.2;
    if (layer == "noodles")
      noodles += 50;
  }
  struct amount result {
    noodles, sauce
  };
  return result;
}

void addSecretIngredient(std::vector<std::string> &mine,
                         const std::vector<std::string> other) {
  mine.back() = other.back();
}
std::vector<double> scaleRecipe(std::vector<double> layers, int scale) {
  double newScale = (double)scale / 2;
  std::vector<double> newArray{};
  if (layers.size() == 0)
    return newArray;
  for (double d : layers)
    newArray.push_back(newScale * d);
  return newArray;
}
void addSecretIngredient(std::vector<std::string> &mine,
                         const std::string other) {
  mine.back() = other;
}
} // namespace lasagna_master
