#pragma once
#include <string>
#include <vector>

namespace lasagna_master
{
    int preparationTime(std::vector<std::string> layers, int minutes = 2);

    struct amount
    {
        int noodles;
        double sauce;
    };
    struct amount quantities(std::vector<std::string> layers);
    void addSecretIngredient(std::vector<std::string> &mine, const std::vector<std::string> other);
    void addSecretIngredient(std::vector<std::string> &mine, std::string other);
    std::vector<double> scaleRecipe(std::vector<double> layers, int scale);

} // namespace lasagna_master
