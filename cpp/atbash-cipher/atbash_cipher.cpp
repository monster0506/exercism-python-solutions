#include "atbash_cipher.h"
using namespace std;

namespace atbash_cipher {
string letters = "abcdefghijklmnopqrstuvwxyz";

char transpose(char c) {
  if (isalpha(c))
    return letters[letters.size() - letters.find_first_of(c) - 1];
  else
    return c;
}

string encode(string plaintext) {
  plaintext = regex_replace(plaintext, regex(R"([\W])"), "");
  for (char &c : plaintext)
    c = transpose(tolower(c));
  for (auto pos = plaintext.begin() + 5; pos < plaintext.end(); pos += 6)
    plaintext.insert(pos, ' ');
  return plaintext;
}

string decode(string cipher) {
  for (char &c : cipher)
    c = transpose(c);
  return regex_replace(cipher, regex(R"([\s])"), "");
}
} // namespace atbash_cipher
