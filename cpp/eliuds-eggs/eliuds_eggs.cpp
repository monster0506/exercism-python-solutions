#include "eliuds_eggs.h"

namespace chicken_coop {
int positions_to_quantity(int num) {
  int count{};
  while (num) {
    count++;
    num &= (num - 1);
  }
  return count;
}

} // namespace chicken_coop
