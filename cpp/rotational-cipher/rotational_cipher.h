#pragma once

#include <cctype>
#include <string>

namespace rotational_cipher {

std::string rotate(std::string str, int amount = 13);
} // namespace rotational_cipher
