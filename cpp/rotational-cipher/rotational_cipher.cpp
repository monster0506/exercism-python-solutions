#include "rotational_cipher.h"
#include <cctype>
namespace rotational_cipher {
const std::string alphabet{"ABCDEFGHIJKLMNOPQRSTUVWXYZ"};
std::string rotate(std::string str, int amount) {
  std::string result{};
  std::string rotated =
      alphabet.substr(amount, 26 - amount) + alphabet.substr(0, amount);

  for (char c : str) {
    if (islower(c)) {
      result.push_back(tolower(rotated.at(alphabet.find_first_of(toupper(c)))));
    } else if (isupper(c)) {
      result.push_back(rotated.at(alphabet.find_first_of(c)));
    } else {
      result.push_back(c);
    }
  }
  return result;
}

} // namespace rotational_cipher
