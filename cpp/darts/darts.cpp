#include "darts.h"

namespace darts {
int score(double x, double y) {
  double location = x * x + y * y;
  if (location > 100)
    return 0;
  if (location > 25)
    return 1;
  if (location > 1)
    return 5;
  return 10;
}
} // namespace darts
