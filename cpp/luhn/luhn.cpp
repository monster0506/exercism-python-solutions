#include "luhn.h"
#include <algorithm>
#include <cctype>

namespace luhn {
bool isnumeric(std::string str) {
  for (char c : str)
    if (!isdigit(c))
      return false;
  return true;
}

bool valid(std::string str) {
  int sum{};
  str.erase(std::remove(str.begin(), str.end(), ' '), str.end());
  if (str.size() < 2 || !isnumeric(str))
    return false;
  for (int i = str.size() - 2; i >= 0; i--) {
    int value = str[i] - '0';
    if (i % 2 == (int)str.size() % 2) {
      value = value * 2;
      if (value > 9) {
        value -= 9;
      }
    }
    sum += value;
  }
  sum += (str.back() - '0');

  return sum % 10 == 0;
}

} // namespace luhn
